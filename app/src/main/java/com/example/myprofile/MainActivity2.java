package com.example.myprofile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    public void run_back(View view) {
        Intent intent2 = new Intent(this,MainActivity.class);
        startActivity(intent2);

    }


    public void back(View view) {
        Intent intent6 = new Intent(this,MainActivity.class);
        startActivity(intent6);
    }
}