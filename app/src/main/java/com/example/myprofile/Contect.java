package com.example.myprofile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class Contect extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contect);
    }

    public void Back(View view) {
        Intent intent5 = new Intent(this,MainActivity.class);
        startActivity(intent5);
    }

    public void ig(View view) {
        Intent intent7 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/fiiq.h/?hl=th"));
        startActivity(intent7);
    }

    public void fb(View view) {
        Intent intent8 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/feena.hayeemaseng.9"));
        startActivity(intent8);
    }
}